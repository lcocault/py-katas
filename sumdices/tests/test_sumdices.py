#!/usr/bin/env python3
""" Test the SumDices classes """

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from unittest import TestCase
from sumdices import sumDices


class TestSumDices(TestCase):

    def test_single_dice_result_length(self):
        sums = sumDices(1)
        self.assertEqual(len(sums), 7)

    def test_single_dice_first_element(self):
        sums = sumDices(1)
        self.assertEqual(sums[1], 1)

    def test_single_dice_mid_element(self):
        sums = sumDices(1)
        self.assertEqual(sums[4], 1)

    def test_single_dice_last_element(self):
        sums = sumDices(1)
        self.assertEqual(sums[6], 1)

    def test_single_dice_count(self):
        sums = sumDices(1)
        self.assertEqual(sums["count"], 6)

    def test_two_dices_result_length(self):
        sums = sumDices(2)
        self.assertEqual(len(sums), 12)

    def test_two_dices_first_element(self):
        sums = sumDices(2)
        self.assertEqual(sums[2], 1)

    def test_two_dices_mid_element(self):
        sums = sumDices(2)
        self.assertEqual(sums[7], 6)

    def test_two_dices_last_element(self):
        sums = sumDices(2)
        self.assertEqual(sums[12], 1)

    def test_two_dices_count(self):
        sums = sumDices(2)
        self.assertEqual(sums["count"], 36)
