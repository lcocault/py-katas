#!/usr/bin/env python3
""" Statistics based on dices sum
Usage:
    See "help"
"""

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from pandas import DataFrame
import plotly.express as px
from sumdices import sumDices


def generateStatsAndGraph(numberOfDices):
    dices = sumDices(numberOfDices)
    count = dices["count"]
    dices.pop("count")
    data = []
    for sum in dices.keys():
        ratio = dices[sum]*100/count
        print("{} dices - {} has {}%".format(numberOfDices, sum, ratio))
        data.append({"sum": sum, "count": dices[sum], "ratio": ratio})
    df = DataFrame(data)
    fig = px.bar(df, x="sum", y="count")
    fig.write_image("{}dices.png".format(numberOfDices), width=500, height=500)


if __name__ == '__main__':
    for i in range(1, 7):
        generateStatsAndGraph(i)
