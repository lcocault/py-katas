#!/usr/bin/env python3
""" Computer of statistics based on sum of dices
Usage:
    See "help"
"""

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


# Count one new occurrence of a value in the dictionary of sums.
# :param sum: Dictionary of sums to complement
# :param value: Value for which the occurrence count must be incremented
def addSumValue(sum, value):
    sum["count"] = sum["count"]+1
    if value in sum:
        sum[value] = sum[value]+1
    else:
        sum[value] = 1


# Recursively compute the sums.
# :param sum: Dictionary of sums to complement
# :param numberOfDices: Number of remaining dices to take into account
# :param base: Current value of the sum of previous dices
def recursiveSum(sum, numberOfDices, base):
    for i in range(1, 7):
        if numberOfDices == 1:
            addSumValue(sum, base+i)
        else:
            recursiveSum(sum, numberOfDices-1, base+i)


# Entry point of the sum computation
# :param numberOfDices: Number of dices to take into account
def sumDices(numberOfDices):
    sum = {"count": 0}
    recursiveSum(sum, numberOfDices, 0)
    return sum
