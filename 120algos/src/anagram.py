#!/usr/bin/env python3
""" Compute anagrams in a phrase
Usage:
    See "help"
"""

#    Copyright 2020 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


# Compute a signature of a word, a word containing the characters of the
# original word in the lexicographic order.
# :param word: Word for which a signature must be computed
# :return: Signature of the given word
def signature(word):
    return sorted(word)


# Check if one word is the anagram of another word
# :param first: First word to check
# :param second: Second word to check
# :return: True if the first word  is the anagram of second word
def isAnagram(first, second):
    return signature(first) == signature(second)


# Remive the duplicated words in the given phrase
# :param phrase: Phrase to process
# :return: The phrase without duplicated words
def removeDuplicates(phrase):
    noDuplicatePhrase = set()
    for word in phrase.split():
        noDuplicatePhrase.add(word)
    return sorted(noDuplicatePhrase)


# Group the anagrams in a given sentence.
# :param phrase: Phrase to analyze
# :return: Array of groups of anagrams found
def extractAnagrams(phrase):
    result = []
    for word in removeDuplicates(phrase):
        match_found = False
        for token in result:
            if isAnagram(token[0], word):
                token.append(word)
                match_found = True
        if match_found is False:
            result.append([word])
    return [group for group in result if len(group) > 1]
