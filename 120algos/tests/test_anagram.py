#!/usr/bin/env python3
""" Test the 001 algorithm about anagrams """

#    Copyright 2020 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from unittest import TestCase
from anagram import extractAnagrams


class TestAnagram(TestCase):

    def test_single_word(self):
        anagrams = extractAnagrams("this")
        self.assertEqual(len(anagrams), 0)

    def test_two_words_non_anagrams(self):
        anagrams = extractAnagrams("this test")
        self.assertEqual(len(anagrams), 0)

    def test_two_words_identical(self):
        anagrams = extractAnagrams("this this")
        self.assertEqual(len(anagrams), 0)

    def test_two_words_anagrams(self):
        anagrams = extractAnagrams("this shit")
        self.assertEqual(len(anagrams), 1)
        self.assertEqual(len(anagrams[0]), 2)
        self.assertEqual(anagrams[0][0], "shit")
        self.assertEqual(anagrams[0][1], "this")

    def test_two_anagrams_one_single(self):
        anagrams = extractAnagrams("test this shit")
        self.assertEqual(len(anagrams), 1)
        self.assertEqual(len(anagrams[0]), 2)
        self.assertEqual(anagrams[0][0], "shit")
        self.assertEqual(anagrams[0][1], "this")

    def test_two_anagrams_two_singles(self):
        anagrams = extractAnagrams("eat and drink tea")
        self.assertEqual(len(anagrams), 1)
        self.assertEqual(len(anagrams[0]), 2)
        self.assertEqual(anagrams[0][0], "eat")
        self.assertEqual(anagrams[0][1], "tea")

    def test_full_example(self):
        anagrams = extractAnagrams("le chien marche vers sa niche et trouve une limace de chine nue pleine de malice qui lui fait du charme")
        self.assertEqual(len(anagrams), 4)
        self.assertEqual(len(anagrams[0]), 2)
        self.assertEqual(anagrams[0][0], "charme")
        self.assertEqual(anagrams[0][1], "marche")
        self.assertEqual(len(anagrams[1]), 3)
        self.assertEqual(anagrams[1][0], "chien")
        self.assertEqual(anagrams[1][1], "chine")
        self.assertEqual(anagrams[1][2], "niche")
        self.assertEqual(len(anagrams[2]), 2)
        self.assertEqual(anagrams[2][0], "limace")
        self.assertEqual(anagrams[2][1], "malice")
        self.assertEqual(len(anagrams[3]), 2)
        self.assertEqual(anagrams[3][0], "nue")
        self.assertEqual(anagrams[3][1], "une")
